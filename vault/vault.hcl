# See https://www.vaultproject.io/docs/configuration for more details about configuration options

ui = true

storage "file" {
  path = "/opt/vault"
}

# HTTP listener (insecure)
listener "tcp" {
  address = "127.0.0.1:8200"
  tls_disable = 1
}

## HTTPS listener
#listener "tcp" {
#  address       = "0.0.0.0:8200"
#  tls_cert_file = "tls.crt"
#  tls_key_file  = "tls.key"
#}

## Auto Unseal via Yandex Key Management Service (see https://cloud.yandex.ru/docs/kms/solutions/vault-secret for more details)
seal "yandexcloudkms" {
  kms_key_id = "abjo3nbtri2ojdki650r"
  oauth_token = "y0_AgAAAAAHGjRyAATuwQAAAADZqDDciC3rf8HVRE-kG2rA1rYAYVASPVA"
}
